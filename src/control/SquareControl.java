package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.King;
import model.Piece;
import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_CAN_MOVE = Color.CYAN;
	public static final Color DEFAULT_COLOR_CAN_MOVE_SELECTED = Color.BLACK;
	public static final Color DEFAULT_COLOR_CAN_CAPTURE = Color.MAGENTA;

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Integer countPlay ;

	private Square selectedSquare;
	private ArrayList<Square> squareList;


	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.squareList = new ArrayList<>();
		this.countPlay = 0;
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
//System.out.println(square.getPosition());
		if(square.getColor() == DEFAULT_COLOR_CAN_MOVE) {
			square.setColor(DEFAULT_COLOR_CAN_MOVE_SELECTED);
		}
		else {
			square.setColor(this.colorHover);
		}		
	}

	@Override
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) {
		
				if (!this.selectedSquare.equals(square) ) {
					aplyMove(square);
				} else {
					unselectSquare(square);
				}
		
		} else {
		try{
			selectSquare(square);
		}catch(CheckMove erro){
			erro.message("É a vez do outro jogador");
		}
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if(square.getColor() != DEFAULT_COLOR_CAN_MOVE_SELECTED){
			if (this.selectedSquare != square) {
				resetColor(square);
			} else {
				square.setColor(this.colorSelected);
			}
		}
		else
			square.setColor(DEFAULT_COLOR_CAN_MOVE);
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {	 
		return this.selectedSquare != EMPTY_SQUARE;
	}

	public String getCorPt(String cor){
		if(cor.equalsIgnoreCase("white")){
			return "BRANCO";
		}else
			return "PRETO";
	}
	private void moveContentOfSelectedSquare(Square square) {
		if(square.getPiece() != null && square.getPiece().getClass() == King.class){
			JOptionPane.showMessageDialog(null, "O jogador "+getCorPt(this.getSelectedSquare().getPiece().getColor())+" ganhou!");
			System.exit(0);
		}
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
		countPlay++;
	}

	private void selectSquare(Square square) throws CheckMove{
		if (square.havePiece()){
			if(	(countPlay%2==0 && isWhite(square.getPosition())) || 
				(countPlay%2!=0 && !isWhite(square.getPosition()))) {
			this.selectedSquare=square;
			Piece piece = square.getPiece();
			if(applyGamePhase(countPlay)) {
				piece.move(this,DEFAULT_COLOR_CAN_MOVE);
			}
		}else
			throw new CheckMove();
		}
	}


	public Square getSelectedSquare() {
		return selectedSquare;
	}

	public void setSelectedSquare(Square selectedSquare) {
		this.selectedSquare = selectedSquare;
	}

	private void unselectSquare(Square square) {
		unShow(squareList);
		resetColor(this.selectedSquare);
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}

	public void unShow(ArrayList<Square> squareList) {
		Square square;
		for(int i=0; i<64;i++) {
			square = squareList.get(i);
			resetColor(square);
		}
	}

	public void aplyMove(Square square) {
		if(square.getColor() == DEFAULT_COLOR_CAN_MOVE_SELECTED) {
				moveContentOfSelectedSquare(square);
			}
	}
	
	public boolean isWhite(int x, int y) {
		boolean colorPiece;
		if(getSquare(x, y).getPiece().getColor() == "white") {
			colorPiece = true;
		} else {
			colorPiece = false;
		}
		return colorPiece;
	}


	public boolean isWhite(Point ponto) {
		boolean colorPiece;
		if(getSquare(ponto.x,ponto.y).getPiece().getColor() == "white") {
			colorPiece = true;
		} else {
			colorPiece = false;
		}
		return colorPiece;
	}
	
	public boolean applyGamePhase(int countPlay) {
		Point point;
		point = getSelectedSquare().getPosition();
		boolean flag = false;
		if(countPlay % 2 == 0) {
			if(isWhite(point.x, point.y)) {
				flag = true;
			}
		} else if (!isWhite(point.x, point.y)) {
			flag = true;
		}
		return flag;
	}
	
	public boolean isNull(int x, int y) {
		boolean isnull = false;
		
		if(getSquare(x, y).getPiece() == null) {
			isnull = true;
		}
		
		return isnull;
	}
}

