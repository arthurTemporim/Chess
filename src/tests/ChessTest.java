package tests;

import java.awt.Point;
import java.util.ArrayList;

import model.Bishoop;
import model.King;
import model.Knight;
import model.Pawn;
import model.Piece;
import model.Queen;
import model.Rook;
import model.Square;

import org.junit.Assert;
import org.junit.Test;

import control.SquareControl;


public class ChessTest {
	
	int x = MovesTestHelper.POSITION_X;
	int y = MovesTestHelper.POSITION_Y;

	//Cria square control
	SquareControl squareControl = new SquareControl();
	
	@Test
	public void testPawnMove() throws Exception {	

		Piece pawn = new Pawn("1");//Inicializa peça
		
		squareControl.getSquare(x,y).setPiece(pawn);//adiciona peça no quadrado
		squareControl.setSelectedSquare(squareControl.getSquare(x, y));//seleciona quadrado pro método move
		pawn.move(squareControl, squareControl.DEFAULT_COLOR_CAN_MOVE);//pinta os quadrados que pode mover
		ArrayList<Point> pontosPossiveis = new ArrayList<Point>();//inicaliza lista de pontos
		
		for(Square square : squareControl.getSquareList()){
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());//adiciona na lista as posições pintadas
		}
		
		assertMoves(MovesTestHelper.getPawnMoves(pawn), pontosPossiveis);//faz o teste
		
		pawn.setPlayer("2");//muda para pawn do outro time
		pawn.setColor("black");
		
		squareControl.unShow(squareControl.getSquareList());//"despinta" os quadrados
		pontosPossiveis.removeAll(pontosPossiveis);//limpa lista de posições
		
		pawn.move(squareControl,squareControl.DEFAULT_COLOR_CAN_MOVE);//marca os quadrados
		
		for(Square square : squareControl.getSquareList()){	
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());//adiciona na lista as posições que pode mover
		}
		
		assertMoves(MovesTestHelper.getPawnMoves(pawn),pontosPossiveis);//faz o teste
	}
	

	@Test
	public void testRookMove() throws Exception {
		
		Piece rook = new Rook("1");
		
		squareControl.getSquare(x,y).setPiece(rook);
		squareControl.setSelectedSquare(squareControl.getSquare(x, y));
		rook.move(squareControl, squareControl.DEFAULT_COLOR_CAN_MOVE);
		ArrayList<Point> pontosPossiveis = new ArrayList<Point>();
		
		for(Square square : squareControl.getSquareList()){
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());
		}
		
		assertMoves(MovesTestHelper.getRookMoves(), pontosPossiveis);//faz o teste
		
	}

	
	@Test
	public void testBishoopMove() throws Exception {

		Piece bishoop = new Bishoop("1");
		
		squareControl.getSquare(x,y).setPiece(bishoop);
		squareControl.setSelectedSquare(squareControl.getSquare(x, y));
		bishoop.move(squareControl, squareControl.DEFAULT_COLOR_CAN_MOVE);
		ArrayList<Point> pontosPossiveis = new ArrayList<Point>();
		
		for(Square square : squareControl.getSquareList()){
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());
		}
		
		assertMoves(MovesTestHelper.getBishoopMoves(), pontosPossiveis);//faz o teste
		
	}
	
	@Test
	public void testKingMove() throws Exception {

		Piece king = new King("1");
		
		squareControl.getSquare(x,y).setPiece(king);
		squareControl.setSelectedSquare(squareControl.getSquare(x, y));
		king.move(squareControl, squareControl.DEFAULT_COLOR_CAN_MOVE);
		ArrayList<Point> pontosPossiveis = new ArrayList<Point>();
		
		for(Square square : squareControl.getSquareList()){
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());
		}
		
		assertMoves(MovesTestHelper.getKingMoves(), pontosPossiveis);

	}

	@Test
	public void testQueenMove() throws Exception {

		Piece queen = new Queen("1");
		
		squareControl.getSquare(x,y).setPiece(queen);
		squareControl.setSelectedSquare(squareControl.getSquare(x, y));
		queen.move(squareControl, squareControl.DEFAULT_COLOR_CAN_MOVE);
		ArrayList<Point> pontosPossiveis = new ArrayList<Point>();
		
		for(Square square : squareControl.getSquareList()){
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());
		}
		
		assertMoves(MovesTestHelper.getQueenMoves(), pontosPossiveis);
	}

	@Test
	public void testKnightMove() throws Exception {

		Knight knight = new Knight("1");
		
		squareControl.getSquare(x,y).setPiece(knight);
		squareControl.setSelectedSquare(squareControl.getSquare(x, y));
		knight.move(squareControl, squareControl.DEFAULT_COLOR_CAN_MOVE);
		ArrayList<Point> pontosPossiveis = new ArrayList<Point>();
		
		for(Square square : squareControl.getSquareList()){
			if(square.getColor() == squareControl.DEFAULT_COLOR_CAN_MOVE)
				pontosPossiveis.add(square.getPosition());
		}
		
		assertMoves(MovesTestHelper.getKnightMoves(), pontosPossiveis);
		
	}
	
	private void assertMoves(ArrayList<Point> movesTest, ArrayList<Point> movesChess)
			throws Exception{
		for(Point ponto : movesChess){
			if(!movesTest.contains(ponto)){
				Assert.assertTrue(false);
				return;
			}
		}
		Assert.assertTrue(true);
	}
}
