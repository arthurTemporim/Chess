package model;

import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

public class Pawn extends Piece{

	public Pawn() {
		super();
	}
	public Pawn(String player) {
		super.name = "pawn";
		super.player=player;
		selectPlayer(player);
	}

	public void selectPlayer(String player) {
		if(player == "1") {
			super.image = "icon/White P_48x48.png";
			super.color = "white";
		} else {
			super.image = "icon/Black P_48x48.png";
			super.color = "black";
		}
	}
	
	public void move(SquareControl squareControl, Color cor) {
		Point point = squareControl.getSelectedSquare().getPosition();
//		if(point.x+1 <= 7  && point.x-1 >= 0 && point.y+1 <= 7 && point.y-1 <=0) {
			if(!squareControl.isWhite(point.x, point.y)) {
				//if(point.x+1 <= 7  && point.x-1 >= 0 && point.y <= 7 && point.y <=0) {
				if(squareControl.isNull(point.x+1, point.y)) {
					squareControl.getSquare(point.x+1, point.y).setColor(cor);
					if(point.x == 1 && squareControl.isNull(point.x+2, point.y)) {
						squareControl.getSquare(point.x+2, point.y).setColor(cor);
					}
				}
				if(!squareControl.isNull(point.x+1, point.y+1)) {
					if(squareControl.isWhite(point.x+1, point.y+1)) {
						squareControl.getSquare(point.x+1, point.y+1).setColor(cor);
					}
				}

				if(squareControl.getSquare(point.x+1, point.y-1).getPiece() != null) {
					if(squareControl.isWhite(point.x+1, point.y-1)) {
						squareControl.getSquare(point.x+1, point.y-1).setColor(cor);
					}
				}
				//}

			} else {
				//if(point.x-1 >= 0) {

				if(squareControl.getSquare(point.x-1, point.y).getPiece() == null) {
					squareControl.getSquare(point.x-1, point.y).setColor(cor);
					if(point.x == 6 && squareControl.getSquare(point.x-2, point.y).getPiece() == null) {
						squareControl.getSquare(point.x-2, point.y).setColor(cor);
					}
				}
				if(squareControl.getSquare(point.x-1, point.y+1).getPiece() != null) {
					if(!squareControl.isWhite(point.x-1, point.y+1)) {
						squareControl.getSquare(point.x-1, point.y+1).setColor(cor);
					}
				}

				if(squareControl.getSquare(point.x-1, point.y-1).getPiece() != null) {
					if(!squareControl.isWhite(point.x-1, point.y-1)) {
						squareControl.getSquare(point.x-1, point.y-1).setColor(cor);
					}
				}
				//}
			}
		}
	}

