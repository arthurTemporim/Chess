package model;

import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

public class Bishoop extends Piece{

	public Bishoop() {
		super();
	}
	public Bishoop(String player) {
		super.name = "bishoop";
		selectPlayer(player);
	}

	public void selectPlayer(String player) {
		if(player == "1") {
			super.image = "icon/White B_48x48.png";
			super.color = "white";
		} else {
			super.image = "icon/Black B_48x48.png";
			super.color = "black";
		} 
	}

	public void move(SquareControl squareControl, Color cor) {
		Point point  = squareControl.getSelectedSquare().getPosition();
		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++) {					
			if(squareControl.isNull(i, j)){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++) {		
			if(squareControl.isNull(i, j)){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--) {		
			if(squareControl.isNull(i, j)){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--) {		
			if(squareControl.isNull(i,j)){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
	}
}
