package model;

import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

public class Queen extends Piece{

	public Queen() {
		super();
	}
	public Queen(String player) {
		super.name = "queen";
		selectPlayer(player);
	}

	public void selectPlayer(String player) {
		if(player == "1") {
			super.image = "icon/White Q_48x48.png";
			super.color = "white";
		} else {
			super.image = "icon/Black Q_48x48.png";
			super.color = "black";
		}
	}
	public void move(SquareControl squareControl, Color cor) {
		Point point = squareControl.getSelectedSquare().getPosition();
		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++) {					
			if(squareControl.getSquare(i,j).getPiece()==null){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++) {		
			if(squareControl.getSquare(i,j).getPiece()==null){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--) {		
			if(squareControl.getSquare(i,j).getPiece()==null){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--) {		
			if(squareControl.getSquare(i,j).getPiece()==null){
				squareControl.getSquare(i,j).setColor(cor);
			}
			else if(squareControl.getSquare(i,j).getPiece().getColor() != color) {
				squareControl.getSquare(i,j).setColor(cor);
				break;
			}
			else{
				break;
			}
		}
		for(int i = point.y+1; i < SquareControl.COL_NUMBER; i++) {
			if (squareControl.getSquare(point.x,i).getPiece() == null) {
				squareControl.getSquare(point.x,i).setColor(cor);
			}
			else if (squareControl.getSquare(point.x,i).getPiece().getColor() != color) {
				squareControl.getSquare(point.x,i).setColor(cor);
				break;
			}
			else {
				break;
			}
		}
		for(int i = point.y-1; i>=0;i--) {
			if (squareControl.getSquare(point.x, i).getPiece() == null) {
				squareControl.getSquare(point.x,i).setColor(cor);
			}
			else if(squareControl.getSquare(point.x, i).getPiece().getColor() != color) {
				squareControl.getSquare(point.x, i).setColor(cor);
				break;
			}
			else {
				break;
			}
		}
		for(int i = point.x+1; i < SquareControl.COL_NUMBER; i++) {
			if (squareControl.getSquare(i,point.y).getPiece() == null) {
				squareControl.getSquare(i,point.y).setColor(cor);
			}
			else if (squareControl.getSquare(i,point.y).getPiece().getColor() != color) {
				squareControl.getSquare(i,point.y).setColor(cor);
				break;
			}
			else {
				break;
			}
		}
		for(int i = point.x-1; i>=0;i--) {
			if (squareControl.getSquare(i,point.y).getPiece() == null) {
				squareControl.getSquare(i,point.y).setColor(cor);
			}
			else if (squareControl.getSquare(i,point.y).getPiece().getColor() != color) {
				squareControl.getSquare(i,point.y).setColor(cor);
				break;
			}
			else {
				break;
			}
		}
	}
}