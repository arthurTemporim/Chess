package model;

import java.awt.Color;

import control.SquareControl;

public abstract class Piece {

	protected String name;
	protected String image;
	protected String color;
	protected String player;
	
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player  = player;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public Piece() {
		super();
	}
	
	public Piece(String name, String image, String color, String player) {
		super();
		this.name = name;
		this.image = image;
		this.color = color;
		this.player  = player;
	}
	
	public abstract void selectPlayer(String player);
	public abstract void move(SquareControl squareControl,Color cor);
}
