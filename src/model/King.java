package model;

import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

public class King extends Piece{
	
	public King() {
		super();
	}
	public King(String player) {
		super.name = "king";
		selectPlayer(player);
	}

	public void selectPlayer(String player) {
		if(player == "1") {
			super.image = "icon/White K_48x48.png";
			super.color = "white";
		} else {
			super.image = "icon/Black K_48x48.png";
			super.color = "black";
		}
	}
	public void move(SquareControl squareControl, Color cor) {
		Point point = squareControl.getSelectedSquare().getPosition();
		if(point.x<7 && point.y<7){
			setColorInSquare(point.x+1, point.y+1, cor, squareControl);
			
		}
		if(point.x<7){
			setColorInSquare(point.x+1, point.y, cor, squareControl);
		}
		if(point.x<7 && point.y>0){
			setColorInSquare(point.x+1, point.y-1, cor, squareControl);
		}
		if(point.y<7){
			setColorInSquare(point.x, point.y+1, cor, squareControl);
		}
		if(point.x>0 && point.y>0){
			setColorInSquare(point.x-1, point.y-1, cor, squareControl);
		}
		if(point.x>0){
			setColorInSquare(point.x-1, point.y, cor, squareControl);
		}
		if(point.x>0 && point.y<7){
			setColorInSquare(point.x-1, point.y+1, cor, squareControl);
		}
		if(point.y>0){
			setColorInSquare(point.x, point.y-1, cor, squareControl);
		}
	}
	
	private void setColorInSquare(int x, int y, Color cor, SquareControl squareControl) {
		if(!squareControl.isNull(x, y)) { 
				if (squareControl.getSquare(x,y).getPiece().getColor()!=color){
					squareControl.getSquare(x,y).setColor(cor);
				}
		}else{
			squareControl.getSquare(x, y).setColor(cor);
		}
	}
	
}

