package model;

import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

public class Rook extends Piece{

	public Rook() {
		super();
	}

	public Rook(String player) {
		super.name = "rook";
		selectPlayer(player);
	}

	public void selectPlayer(String player) {
		if(player == "1") {
			super.image = "icon/White R_48x48.png";
			super.color = "white";
		} else {
			super.image = "icon/Black R_48x48.png";
			super.color = "black";
		}
	}

	SquareControl squareControl;

	public void move(SquareControl squareControl,Color cor) {
		this.squareControl = squareControl;
		Point point = squareControl.getSelectedSquare().getPosition();
		int a;
		for(int i = point.y+1; i < SquareControl.COL_NUMBER; i++) {
			a = squareControl.getSelectedSquare().getPosition().x;
			if(doMove(a, i, cor)) {
				break;
			}
		}
		for(int i = point.y-1; i>=0;i--) {
			a = squareControl.getSelectedSquare().getPosition().x;
			if(doMove(a, i, cor)) {
				break;
			}
		}
		for(int i = point.x+1; i < SquareControl.COL_NUMBER; i++) {
			a = squareControl.getSelectedSquare().getPosition().y;
			if(doMove(i, a, cor)) {
				break;
			}
		}
		for(int i = point.x-1; i>=0;i--) {
			a = squareControl.getSelectedSquare().getPosition().y;
			if(doMove(i, a, cor)) {
				break;
			}
		}
	}

	public boolean doMove(int a, int b, Color cor){
		boolean flag = false;
		if(squareControl.getSquare(a, b).getPiece() == null) {
			squareControl.getSquare(a,b).setColor(cor);
		} else if (squareControl.getSquare(a, b).getPiece().getColor() != color) {
			squareControl.getSquare(a, b).setColor(cor);
			flag = true;
		}
		else {
			flag = true;
		}
		return flag;
	}
}