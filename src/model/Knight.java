package model;

import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

public class Knight extends Piece {
	
	SquareControl squareControl;
	public Knight() {
		super();
	}
	public Knight(String player) {
		super.name = "knight";
		selectPlayer(player);
	}

	public void selectPlayer(String player) {
		
		if(player == "1") {
			super.image = "icon/White N_48x48.png";
			super.color = "white";
		} else {
			super.image = "icon/Black N_48x48.png";
			super.color = "black";
		}
	}
	public void move(SquareControl squareControl, Color cor) {
		Point point  = squareControl.getSelectedSquare().getPosition();
		this.squareControl = squareControl;
		if(point.y<6 && point.x<7){
			if(doMove(point.x+1, point.y+2)){
				squareControl.getSquare(point.x+1,point.y+2).setColor(cor);
			}
		}
		if(point.y>1 && point.x<7){
			if(doMove(point.x+1, point.y-2)){
				squareControl.getSquare(point.x+1,point.y-2).setColor(cor);
			}
		}
		if(point.y<6 && point.x>=1){
			if(doMove(point.x-1, point.y+2)){
				squareControl.getSquare(point.x-1,point.y+2).setColor(cor);
			}
		}
		if(point.y>1 && point.x>=1){
			if(squareControl.getSquare(point.x-1,point.y-2).getPiece()==null || squareControl.getSquare(point.x-1,point.y-2).getPiece().getColor()!=color){
				squareControl.getSquare(point.x-1,point.y-2).setColor(cor);
			}
		}
		
		if(point.x>1 && point.y>0){
			if(doMove(point.x-2, point.y-1)){
				squareControl.getSquare(point.x-2,point.y-1).setColor(cor);
			}
		}
		if(point.x>1 && point.y<7){
			if(doMove(point.x-2, point.y+1)){
				squareControl.getSquare(point.x-2,point.y+1).setColor(cor);
			}
		}
		if(point.x<6 && point.y>0){
			if(doMove(point.x+2, point.y-1)){
				squareControl.getSquare(point.x+2,point.y-1).setColor(cor);
			}	
		}
		if(point.x<6 && point.y<7){
			if(doMove(point.x+2, point.y+1)){
				squareControl.getSquare(point.x+2,point.y+1).setColor(cor);
			}	
		}	
	}
	private boolean doMove(int a, int b) {
		boolean reto = false;
		if(squareControl.getSquare(a,b).getPiece()==null || squareControl.getSquare(a,b).getPiece().getColor()!=color){
			reto = true;
		}
		return reto;
	}
}