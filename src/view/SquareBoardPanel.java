package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Bishoop;
import model.King;
import model.Knight;
import model.Pawn;
import model.Queen;
import model.Rook;
import model.Square;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
//Setando o layout.
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
//Passando a posição de cada quadrado.
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
//Colocando torres no tabuleiro.
//BLACK
		Rook rookBlack = new Rook("2");
		this.squareControl.getSquare(0, 0).setPiece(rookBlack);
		this.squareControl.getSquare(0,7).setPiece(rookBlack);	

//WHITE
		Rook rookWhite = new Rook("1");
		this.squareControl.getSquare(7, 0).setPiece(rookWhite);
		this.squareControl.getSquare(7,7).setPiece(rookWhite);
		
//Colocando Bispos no tabuleiro.
//BLACK
		Bishoop bishoopBlack = new Bishoop("2");
		this.squareControl.getSquare(0, 2).setPiece(bishoopBlack);
		this.squareControl.getSquare(0, 5).setPiece(bishoopBlack);
		
//WHITE
		Bishoop bishoopWite = new Bishoop("1");
		this.squareControl.getSquare(7, 2).setPiece(bishoopWite);
		this.squareControl.getSquare(7, 5).setPiece(bishoopWite);
		
//Colocando cavalos no tabuleiro.
//BLACK
		Knight knightBlack = new Knight("2");
		this.squareControl.getSquare(0, 1).setPiece(knightBlack);
		this.squareControl.getSquare(0, 6).setPiece(knightBlack);
		
//WHITE
		Knight knightWhite = new Knight("1");
		this.squareControl.getSquare(7, 1).setPiece(knightWhite);
		this.squareControl.getSquare(7, 6).setPiece(knightWhite);
		
//Colocando Rainhas no tabuleiro.
//BLACK
		Queen queenBlack = new Queen("2");
		this.squareControl.getSquare(0, 3).setPiece(queenBlack);

//WHITE
		Queen queenWhite = new Queen("1");
		this.squareControl.getSquare(7, 3).setPiece(queenWhite);
		
//Colocanfo Peoes no tabuleiro.
//BLACK
		Pawn pawnBlack = new Pawn("2");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(pawnBlack);
		}
		
		
//White
		Pawn pawnWhite = new Pawn("1");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(pawnWhite);
		}
		
//Colocando Reis no tabuleiro.
//BLACK
		King kingBlack = new King("2");
		this.squareControl.getSquare(0, 4).setPiece(kingBlack);
		
		
//WHITE
		King kingWhite = new King("1");
		this.squareControl.getSquare(7, 4).setPiece(kingWhite);
	}
}
