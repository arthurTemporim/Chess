package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class GameFrame extends JFrame {

	private static final long serialVersionUID = -6399886456682347905L;

	public GameFrame() {
		super("Chess");

		initializeScreen();
	}

	public void initializeScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
//Adiciona o painel.
		add(new SquareBoardPanel());
//Remimensiona a tela pra caber todo mundo.
		pack();
//Não sobrescreve a posição, se for relativo.
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
